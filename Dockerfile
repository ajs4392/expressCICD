# https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

FROM node:10.4.1-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]